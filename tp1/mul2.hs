-- mul2 :: Int -> Int

mul2 :: Int -> Int
mul2 x = 2 * x

-- compil like gcc $ ghc -Wall -O2 file.hs

main :: IO ()
main = do
    print $ mul2 21
    print $ mul2 (-42)